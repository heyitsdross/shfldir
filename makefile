cc=gcc
ccflags=
obj=shfldir.c
targets=build


all: shfldir

clean:
	rm -rf $(targets)

shfldir: shfldir.c
	$(cc) -o $@ $< $(ccflags)
