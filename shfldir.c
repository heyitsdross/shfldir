#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]) {

	int opt;

	while ((opt = getopt(argc, argv, "ilw")) != -1) {
		switch(opt) {
		case 'i': printf("Case i\n"); break;
		case 'l': printf("Case l\n"); break;
		case 'w': printf("Case w\n"); break;	
		default:
			fprintf(stderr, "Usage: blahblahblah\n");
			exit(1);
		}
	}

  return 0;
}
